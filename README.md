# RabbitRPC

Simple RPC with RabbitMQ
===

| Branch | Pipe | Coverage |
| :----: | :--: | :------: |
| Development | [![pipeline status](https://gitlab.com/fernandogodinho/rabbitrpc/badges/development/pipeline.svg)](https://gitlab.com/fernandogodinho/rabbitrpc/commits/development) | [![coverage report](https://gitlab.com/fernandogodinho/rabbitrpc/badges/development/coverage.svg)](https://gitlab.com/fernandogodinho/rabbitrpc/commits/development) |
| Staging | [![pipeline status](https://gitlab.com/fernandogodinho/rabbitrpc/badges/staging/pipeline.svg)](https://gitlab.com/fernandogodinho/rabbitrpc/commits/staging) | [![coverage report](https://gitlab.com/fernandogodinho/rabbitrpc/badges/staging/coverage.svg)](https://gitlab.com/fernandogodinho/rabbitrpc/commits/staging) |
| Master | [![pipeline status](https://gitlab.com/fernandogodinho/rabbitrpc/badges/master/pipeline.svg)](https://gitlab.com/fernandogodinho/rabbitrpc/commits/master) | [![coverage report](https://gitlab.com/fernandogodinho/rabbitrpc/badges/master/coverage.svg)](https://gitlab.com/fernandogodinho/rabbitrpc/commits/master) |

Installation
---
```
npm i -S rabbitRPC
```
USAGE
---
Producer example ( Request )
```javascript
const rabbitRPC = require('rabbitRPC');

const server = new rabbitRPC();

server.connect('amqp://localhost:5672')
    .then(() => server.createEndpoint('some-unique-rpc-queue-name'))
    .then(endpoint => endpoint.request('some message'))
    .then(response => console.log(response));
```

Consumer example ( Response )
```javascript
const rabbitRPC = require('rabbitRPC');

const server = new rabbitRPC();

server.connect('amqp://localhost:5672')
    .then(() => server.createEndpoint('some-unique-rpc-queue-name'))
    .then(endpoint => endpoint.respond((request, resolve) => {
        resolve(`${ request } received`);
    }));
```