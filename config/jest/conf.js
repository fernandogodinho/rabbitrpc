const path = require('path')

module.exports = {
  rootDir: path.resolve(__dirname, '../../'),
  setupTestFrameworkScriptFile: '<rootDir>/config/jest/init.js',
  moduleFileExtensions: [ 'js' ],

  testMatch: [
    '**/__tests__/**/*.js',
    '**/*.spec.js'
  ],

  coverageDirectory: '<rootDir>/coverage',
  collectCoverageFrom: [
    'lib/**/*.js',
    '!**/*.spec.js',
    '!**/node_modules/**'
  ]
}
