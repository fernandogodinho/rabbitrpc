const chai = require('chai');
const dirtyChai = require('dirty-chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

const sandbox = sinon.createSandbox();

chai.should();
chai.use(dirtyChai);
chai.use(sinonChai);

global.chai = chai;
global.expect = chai.expect;
global.sinon = sinon;
global.sandbox = sandbox;
