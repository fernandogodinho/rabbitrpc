const name = process.argv.slice(2).join(' ');

console.log(`\x1b[33m${ name ? `${ name } ` : '' }completed by ${ new Date().toLocaleTimeString() }\x1b[0m`);
