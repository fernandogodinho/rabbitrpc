const assertTempQueuePromise = require('./../lib/helpers/assert-temp-queue-promise');

const CONSUME_REQUEST_CONFIG = { noAck: false };

class Endpoint {
    constructor(channel, name) {
        this.request = message =>
            new Promise((resolve, reject) => _generateRequest(channel, name, message, resolve, reject));

        this.respond = handler =>
            channel.consume(name, request => _handleRequest(channel, handler, request), CONSUME_REQUEST_CONFIG);
    }
}

const _generateRequest = (channel, name, message, resolve, reject) =>
    assertTempQueuePromise(channel).then(queueName => {
        const config = { noAck: true, consumerTag: queueName };
        const handler = _handleResponse.bind(this, channel, resolve, queueName);

        channel.consume(queueName, ({ content }) => handler(content), config);

        channel.sendToQueue(name, Buffer.from(message), { replyTo: queueName });
    }).catch(error => reject(error));

const _handleResponse = (channel, resolve, queue, content) => {
    resolve(content);

    channel.cancel(queue);
};

const _handleRequest = (channel, handler, request) => {
    const { content, properties: { replyTo } } = request;

    handler(content, message => {
        channel.ack(request);

        channel.sendToQueue(replyTo, Buffer.from(message));
    });
};

module.exports = Endpoint;
