module.exports = (connection, queueName) =>
    new Promise((resolve, reject) => _createChannel(connection, queueName, resolve, reject));

const _createChannel = (connection, queueName, resolve, reject) => {
    connection.createChannel((error, channel) => _handleChannelCreation(queueName, resolve, reject, error, channel));
};

const _handleChannelCreation = (queueName, resolve, reject, error, channel) => {
    if (error) reject(error);
    else {
        channel.assertQueue(queueName, {}, error => _handleQueueAssertion(channel, resolve, reject, error));
    }
};

const _handleQueueAssertion = (channel, resolve, reject, error) => {
    if (error) {
        channel.close();

        reject(error);
    } else resolve(channel);
};
