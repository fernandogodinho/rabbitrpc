const AUTO_GENERATED_NAME_FLAG = '';
const CONFIG = { exclusive: true, autoDelete: true };

module.exports = channel => new Promise((resolve, reject) =>
    channel.assertQueue(AUTO_GENERATED_NAME_FLAG, CONFIG, (error, q) =>
        error ? reject(error) : resolve(q.queue)
    )
);
