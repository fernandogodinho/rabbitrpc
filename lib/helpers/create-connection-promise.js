const amqp = require('amqplib/callback_api');

module.exports = address => new Promise((resolve, reject) =>
    _createConnection(address, resolve, reject));

const _createConnection = (address, resolve, reject) =>
    amqp.connect(address, (error, connection) =>
        error ? reject(error) : resolve(connection));
