const createConnection = require('./helpers/create-connection-promise');
const createChannel = require('./helpers/create-channel-promise');
const Endpoint = require('./endpoint');

class rabbitRPC {
    constructor() {
        this.connect = address => {
            if (this._connecting) return Promise.reject(Error(rabbitRPC.ERROR_ALREADY_CONNECTING));
            if (this.connection) return Promise.reject(Error(rabbitRPC.ERROR_ALREADY_CONNECTED));

            this._connecting = true;

            return createConnection(address)
                .then(connection => {
                    this._connecting = false;
                    this.connection = connection;
                })
                .catch(error => {
                    this._connecting = false;
                    this.connection = undefined;

                    throw error;
                });
        };

        this.createEndpoint = name => {
            if (!this.connection) return Promise.reject(Error(rabbitRPC.ERROR_NOT_YET_CONNECTED));

            return createChannel(this.connection, name).then(channel =>
                new Endpoint(channel, name)
            );
        };
    }
}

rabbitRPC.ERROR_NOT_YET_CONNECTED = 'Not yet connected!';
rabbitRPC.ERROR_ALREADY_CONNECTING = 'Already connecting!';
rabbitRPC.ERROR_ALREADY_CONNECTED = 'Already connected!';

module.exports = rabbitRPC;
