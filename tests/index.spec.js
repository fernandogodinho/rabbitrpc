const EXPECTED_ADDRESS = 'some expected address';
const EXPECTED_ENDPOINT_NAME = 'some expected endpoint name';
const EXPECTED_CONNECTION = 'some expected connection';
const EXPECTED_CHANNEL = 'some expected channel';
const EXPECTED_ERROR = 'some expected error';

describe('Given the index file', () => {
    let Class, mockedCreateConnectionPromise, createConnectionResolve, createConnectionReject,
        mockedCreateChannelPromise, MockedEndpoint;

    beforeEach(() => {
        mockedCreateConnectionPromise = sandbox.stub();
        jest.doMock('./../lib/helpers/create-connection-promise', () => mockedCreateConnectionPromise);

        mockedCreateChannelPromise = sandbox.stub();
        jest.doMock('./../lib/helpers/create-channel-promise', () => mockedCreateChannelPromise);

        mockedCreateConnectionPromise.returns(new Promise((resolve, reject) => {
            createConnectionResolve = resolve;
            createConnectionReject = reject;
        }));

        MockedEndpoint = class {
            constructor(channel, name) {
                this.channel = channel;
                this.name = name;
            }
        };

        jest.doMock('./../lib/endpoint', () => MockedEndpoint);

        Class = require('../lib/index');
    });

    it('should be a function', () => {
        Class.should.be.an('function');
    });

    describe('when instantiated', () => {
        let instance;

        beforeEach(() => {
            instance = new Class();
        });

        it('should be and instance for the expected class', () => {
            instance.should.be.an.instanceOf(Class);
        });

        describe('and connects with the expected address', () => {
            let actualConnectPromise;

            beforeEach(() => {
                actualConnectPromise = instance.connect(EXPECTED_ADDRESS);
            });

            it('should return a promise', () => {
                actualConnectPromise.should.be.an.instanceOf(Promise);
            });

            describe('and another connection is triggered while still connecting', () => {
                let actualError;

                beforeEach(() => {
                    instance.connect('some other address')
                        .catch(error => { actualError = error.message; });
                });

                it('should throw the expected error', () => {
                    actualError.should.eql(Class.ERROR_ALREADY_CONNECTING);
                });
            });

            describe('and the connection succeeds', () => {
                beforeEach(() => {
                    createConnectionResolve(EXPECTED_CONNECTION);
                });

                it('should create the expected connection', () => {
                    instance.connection.should.equal(EXPECTED_CONNECTION);
                });

                describe('and another connection is triggered after connecting', () => {
                    let actualError;

                    beforeEach(() => {
                        instance.connect('some other address')
                            .catch(error => { actualError = error.message; });
                    });

                    it('should throw an error', () => {
                        actualError.should.eql(Class.ERROR_ALREADY_CONNECTED);
                    });
                });

                describe('and an endpoint is created', () => {
                    let endpointPromise, actualEndpoint;

                    beforeEach(() => {
                        mockedCreateChannelPromise.withArgs(EXPECTED_CONNECTION, EXPECTED_ENDPOINT_NAME)
                            .returns(Promise.resolve(EXPECTED_CHANNEL));

                        endpointPromise = instance.createEndpoint(EXPECTED_ENDPOINT_NAME)
                            .then(endpoint => { actualEndpoint = endpoint; });
                    });

                    it('should return a promise', () => {
                        endpointPromise.should.be.an.instanceOf(Promise);
                    });

                    it('should return an instance of the expected class', () => {
                        actualEndpoint.should.be.an.instanceOf(MockedEndpoint);
                    });

                    it('should have injected the expected channel', () => {
                        actualEndpoint.channel.should.equal(EXPECTED_CHANNEL);
                    });

                    it('should have injected the expected endpoint name', () => {
                        actualEndpoint.name.should.equal(EXPECTED_ENDPOINT_NAME);
                    });
                });

                describe('and an endpoint fails to be created', () => {
                    let actualError;

                    beforeEach(() => {
                        mockedCreateChannelPromise.withArgs(EXPECTED_CONNECTION, EXPECTED_ENDPOINT_NAME)
                            .returns(Promise.reject(EXPECTED_ERROR));

                        instance.createEndpoint(EXPECTED_ENDPOINT_NAME)
                            .catch(error => { actualError = error; });
                    });

                    it('should reject with the expected error', () => {
                        actualError.should.equal(EXPECTED_ERROR);
                    });
                });
            });

            describe('and the connection fails', () => {
                beforeEach(() => {
                    createConnectionReject(EXPECTED_ERROR);
                });

                describe('and a reconnection is attempted', () => {
                    let actualError;

                    beforeEach(() => {
                        mockedCreateConnectionPromise.returns(Promise.resolve(EXPECTED_CONNECTION))
                        instance.connect(EXPECTED_ADDRESS).catch(error => { actualError = error; });
                    });

                    it('should NOT reject the promise', () => {
                        expect(actualError).not.to.exist();
                    });
                });
            });
        });

        describe('and an endpoint is created before a connection is established', () => {
            let actualError;

            beforeEach(() => {
                instance.createEndpoint()
                    .catch(error => { actualError = error.message; });
            });

            it('should throw an error', () => {
                actualError.should.eql(Class.ERROR_NOT_YET_CONNECTED);
            });
        });
    });

    afterEach(() => {
        sandbox.restore();
        jest.resetModules();
    });
});
