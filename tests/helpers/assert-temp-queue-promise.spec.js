const EXPECTED_QUEUE_NAME = 'some expected queue name';
const EXPECTED_QUEUE = { queue: EXPECTED_QUEUE_NAME };
const EXPECTED_QUEUE_CONFIG = { exclusive: true, autoDelete: true };
const EXPECTED_ERROR = 'some expected error';

describe('Given the Temp Queue Promise helper', () => {
    let helper;

    beforeEach(() => {
        helper = require('../../lib/helpers/assert-temp-queue-promise');
    });

    it('should be a function', () => {
        helper.should.be.a('function');
    });

    describe('when invoked with a channel', () => {
        let mockedChannel, actualPromise, actualHandler;

        beforeEach(() => {
            mockedChannel = {
                assertQueue: sandbox.stub()
            };

            mockedChannel.assertQueue.withArgs('', EXPECTED_QUEUE_CONFIG, sinon.match.func)
                .callsFake((name, config, handler) => {
                    actualHandler = handler;
                });

            actualPromise = helper(mockedChannel);
        });

        it('should return a promise', () => {
            actualPromise.should.be.an.instanceOf(Promise);
        });

        describe('and the queue assertion is successfully', () => {
            let actualQueue;

            beforeEach(() => {
                actualPromise.then(queue => { actualQueue = queue; });

                actualHandler(undefined, EXPECTED_QUEUE);
            });

            it('should resolve with the expected queue', () => {
                actualQueue.should.equal(EXPECTED_QUEUE_NAME);
            });
        });

        describe('and the queue assertion fails with an error', () => {
            let actualError;

            beforeEach(() => {
                actualPromise.catch(error => { actualError = error; });

                actualHandler(EXPECTED_ERROR);
            });

            it('should resolve with the expected queue', () => {
                actualError.should.equal(EXPECTED_ERROR);
            });
        });
    });
});
