const EXPECTED_ADDRESS = 'some expected address';
const EXPECTED_ERROR = 'some expected error';
const EXPECTED_CONNECTION = 'some expected connection';

describe('Given the create connection promise helper', () => {
    let helper, mockedAMQP;

    beforeEach(() => {
        mockedAMQP = {
            connect: sinon.stub()
        };

        jest.doMock('amqplib/callback_api', () => mockedAMQP);

        helper = require('../../lib/helpers/create-connection-promise');
    });

    it('should be a function', () => {
        helper.should.be.a('function');
    });

    describe('when invoked', () => {
        let actualPromise, actualHandler;

        beforeEach(() => {
            mockedAMQP.connect.withArgs(EXPECTED_ADDRESS, sinon.match.func)
                .callsFake((address, handler) => { actualHandler = handler; });

            actualPromise = helper(EXPECTED_ADDRESS);
        });

        it('should return a promise', () => {
            actualPromise.should.be.an.instanceOf(Promise);
        });

        it('should try to connect with the expected address', () => {
            mockedAMQP.connect.withArgs(EXPECTED_ADDRESS)
                .should.have.been.calledOnce();
        });

        describe('and the connection fails', () => {
            let actualError;

            beforeEach(() => {
                actualPromise.catch(error => { actualError = error; });

                actualHandler(EXPECTED_ERROR);
            });

            it('should reject the promise with the same error', () => {
                actualError.should.equal(EXPECTED_ERROR);
            });
        });

        describe('and the connection succeeds', () => {
            let actualConnection;

            beforeEach(() => {
                actualPromise.then(connection => { actualConnection = connection; });

                actualHandler(undefined, EXPECTED_CONNECTION);
            });

            it('should resolve with the expected connection', () => {
                actualConnection.should.equal(EXPECTED_CONNECTION);
            });
        });
    });

    afterEach(() => {
        sandbox.restore();
        jest.resetModules();
    });
});
