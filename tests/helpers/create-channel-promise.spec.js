const EXPECTED_QUEUE_NAME = 'some expected queue name';
const EXPECTED_ERROR = 'some expected error';

describe('Given the create channel promise helper', () => {
    let helper;

    beforeEach(() => {
        helper = require('../../lib/helpers/create-channel-promise');
    });

    it('should be a function', () => {
        helper.should.be.a('function');
    });

    describe('when invoked', () => {
        let mockedConnection, actualCreateChannelHandler, actualPromise;

        beforeEach(() => {
            mockedConnection = {
                createChannel: sandbox.stub()
            };

            mockedConnection.createChannel
                .callsFake(handler => { actualCreateChannelHandler = handler; });

            actualPromise = helper(mockedConnection, EXPECTED_QUEUE_NAME);
        });

        it('should return a promise', () => {
            actualPromise.should.be.an.instanceOf(Promise);
        });

        it('should create a new channel', () => {
            mockedConnection.createChannel.should.have.been.calledOnce();
        });

        describe('and the channel creation fails', () => {
            let actualError;

            beforeEach(() => {
                actualPromise.catch(error => { actualError = error; });

                actualCreateChannelHandler(EXPECTED_ERROR);
            });

            it('should resolve with the same error', () => {
                actualError.should.equal(EXPECTED_ERROR);
            });
        });

        describe('and the channel is successfully created', () => {
            let mockedChannel, actualAssertQueueHandler;

            beforeEach(() => {
                mockedChannel = {
                    assertQueue: sandbox.stub(),
                    close: sandbox.stub()
                };

                mockedChannel.assertQueue
                    .callsFake((queueName, config, handler) => { actualAssertQueueHandler = handler; });

                actualCreateChannelHandler(undefined, mockedChannel);
            });

            it('should assert the expected queue', () => {
                mockedChannel.assertQueue.withArgs(EXPECTED_QUEUE_NAME).should.have.been.calledOnce();
            });

            describe('and the queue assertion fails', () => {
                let actualError;

                beforeEach(() => {
                    actualPromise.catch(error => { actualError = error; });

                    actualAssertQueueHandler(EXPECTED_ERROR);
                });

                it('should close the connection', () => {
                    mockedChannel.close.should.have.been.calledOnce();
                });

                it('should reject the promise with the same error', () => {
                    actualError.should.equal(EXPECTED_ERROR);
                });
            });

            describe('and the queue assertion succeeds', () => {
                let actualChannel;

                beforeEach(() => {
                    actualPromise.then(channel => { actualChannel = channel; });

                    actualAssertQueueHandler(undefined);
                });

                it('should NOT close the connection', () => {
                    mockedChannel.close.should.not.have.been.called();
                });

                it('should resolve with the new channel', () => {
                    actualChannel.should.equal(mockedChannel);
                });
            });
        });
    });

    afterEach(() => {
        sandbox.restore();
    });
});
