const EXPECTED_ERROR = 'some expected error';
const EXPECTED_REQUEST_QUEUE_NAME = 'some expected request queue name';
const EXPECTED_RESPONSE_QUEUE_NAME = 'some expected response queue name';
const EXPECTED_REQUEST_MESSAGE = 'some expected request message';
const EXPECTED_RESPONSE_CONSUME_CONFIG = { noAck: true, consumerTag: EXPECTED_RESPONSE_QUEUE_NAME };
const EXPECTED_RESPONSE = 'some expected response';
const EXPECTED_REQUEST_CONSUME_CONFIG = { noAck: false };
const EXPECTED_REQUEST = {
    properties: { replyTo: EXPECTED_RESPONSE_QUEUE_NAME },
    content: EXPECTED_REQUEST_MESSAGE
};

describe('Given the endpoint Class', () => {
    let mockedAssertTempQueuePromise, Class;

    beforeEach(() => {
        mockedAssertTempQueuePromise = sandbox.stub();

        jest.doMock('./../lib/helpers/assert-temp-queue-promise', () => mockedAssertTempQueuePromise);

        Class = require('./../lib/endpoint');
    });

    describe('when instantiated', () => {
        let instance, mockedChannel;

        beforeEach(() => {
            mockedChannel = {
                consume: sandbox.stub(),
                sendToQueue: sandbox.stub(),
                cancel: sandbox.spy(),
                ack: sandbox.spy()
            };

            instance = new Class(mockedChannel, EXPECTED_REQUEST_QUEUE_NAME);
        });

        it('should be an instance of Endpoint', () => {
            instance.should.be.an.instanceOf(Class);
        });

        describe('and a request is made', () => {
            describe('and there is an error asserting the temp queue', () => {
                let actualError;

                beforeEach(() => {
                    mockedAssertTempQueuePromise.withArgs(mockedChannel)
                        .returns(Promise.reject(EXPECTED_ERROR));

                    return instance.request(EXPECTED_REQUEST_MESSAGE)
                        .catch(error => { actualError = error; });
                });

                it('should resolve with the same error', () => {
                    actualError.should.equal(EXPECTED_ERROR);
                });
            });

            describe('and the temp queue is asserted successfully', () => {
                let actualResponse;
                let actualHandler;

                beforeEach(() => {
                    mockedAssertTempQueuePromise.withArgs(mockedChannel)
                        .returns(Promise.resolve(EXPECTED_RESPONSE_QUEUE_NAME));

                    mockedChannel.consume.withArgs(EXPECTED_RESPONSE_QUEUE_NAME, sinon.match.func, EXPECTED_RESPONSE_CONSUME_CONFIG)
                        .callsFake((queue, handler) => { actualHandler = handler; });

                    mockedChannel.sendToQueue.withArgs(EXPECTED_REQUEST_QUEUE_NAME, Buffer.from(EXPECTED_REQUEST_MESSAGE), { replyTo: EXPECTED_RESPONSE_QUEUE_NAME })
                        .callsFake(() => actualHandler({ content: EXPECTED_RESPONSE }));

                    return instance.request(EXPECTED_REQUEST_MESSAGE)
                        .then(response => { actualResponse = response; });
                });

                it('should bind with the response queue', () => {
                    mockedChannel.consume.withArgs(EXPECTED_RESPONSE_QUEUE_NAME, sinon.match.func, EXPECTED_RESPONSE_CONSUME_CONFIG)
                        .should.have.been.calledOnce();
                });

                it('should push the expected message to the expected queue', () => {
                    mockedChannel.sendToQueue.withArgs(EXPECTED_REQUEST_QUEUE_NAME, Buffer.from(EXPECTED_REQUEST_MESSAGE), { replyTo: EXPECTED_RESPONSE_QUEUE_NAME })
                        .should.have.been.calledOnce();
                });

                it('should have closed the temp queue', () => {
                    mockedChannel.cancel.withArgs(EXPECTED_RESPONSE_QUEUE_NAME)
                        .should.have.been.calledOnce();
                });

                it('should resolve with the expected response', () => {
                    actualResponse.should.equal(EXPECTED_RESPONSE);
                });
            });
        });

        describe('and responding to requests', () => {
            let actualConsumeEventHandler, mockedRequestHandler, actualResolve;

            beforeEach(() => {
                mockedChannel.consume.withArgs(EXPECTED_REQUEST_QUEUE_NAME, sinon.match.func, EXPECTED_REQUEST_CONSUME_CONFIG)
                    .callsFake((name, handler, config) => { actualConsumeEventHandler = handler; });

                mockedRequestHandler = sandbox.stub();

                mockedRequestHandler.withArgs(EXPECTED_REQUEST_MESSAGE, sinon.match.func)
                    .callsFake((message, resolve) => { actualResolve = resolve; });

                instance.respond(mockedRequestHandler);
            });

            it('should bind with the request queue', () => {
                mockedChannel.consume.withArgs(EXPECTED_REQUEST_QUEUE_NAME, sinon.match.func, EXPECTED_REQUEST_CONSUME_CONFIG)
                    .should.have.been.calledOnce();
            });

            describe('and a request is made', () => {
                beforeEach(() => {
                    actualConsumeEventHandler(EXPECTED_REQUEST);
                });

                it('should invoke the handler with the expected message', () => {
                    mockedRequestHandler
                        .withArgs(EXPECTED_REQUEST_MESSAGE, sinon.match.func)
                        .should.have.been.calledOnce();
                });

                it('should NOT have acknowledged the request before it is resolved', () => {
                    mockedChannel.ack.should.not.have.been.called();
                });

                it('should NOT have responded before the request is resolved', () => {
                    mockedChannel.sendToQueue.should.not.have.been.called();
                });

                describe('and the request is resolved', () => {
                    beforeEach(() => {
                        actualResolve(EXPECTED_RESPONSE);
                    });

                    it('should acknowledge the request was resolved', () => {
                        mockedChannel.ack.withArgs(EXPECTED_REQUEST)
                            .should.have.been.calledOnce();
                    });

                    it('should send the response to the expected queue', () => {
                        mockedChannel.sendToQueue.withArgs(EXPECTED_RESPONSE_QUEUE_NAME, Buffer.from(EXPECTED_RESPONSE))
                            .should.have.been.calledOnce();
                    });
                });
            });
        });
    });

    afterEach(() => {
        sandbox.restore();
        jest.resetModules();
    });
});
