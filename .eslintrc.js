// https://eslint.org/docs/user-guide/configuring

module.exports = {
  root: true,
  parserOptions: {
    parser: 'babel-eslint'
  },
  env: {
    browser: false,
  },
  extends: [
    'standard'
  ],
  rules: {
    semi: [ "error", "always" ],
    indent: [ "error", 4 ],
    "template-curly-spacing": [ "error", "always" ],
    "space-before-function-paren": ["error", "never"]
  },
  "overrides": [
    {
      "files": [
        "**/__tests__/**/*",
        "*.spec.js"
      ],
      "env": {
        "mocha": true,
        "jest": true
      },
      "globals": {
        "expect": true,
        "sinon": true,
        "sandbox": true
      },
      "rules": {
        "import/first": 0,
        "import/order": ["error", {
          "groups": [ "builtin", "external", "internal", "parent", "sibling", "index" ],
          "newlines-between": "always"
        }]
      }
    }
  ]
}
